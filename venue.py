# # Bokeh
# from bokeh.plotting import figure, show
# from bokeh.transform import cumsum
# from bokeh.palettes import Category20c

# # Pandas
# import pandas as pd

# # Math
# from math import pi

# # Importing Dataset
# df = pd.read_excel("data/ronaldo.xlsx")

# # Data Analysis
# result_df = df["Venue"].value_counts().reset_index()
# result_df.columns = ['Venue', 'Goals']

# data = result_df
# data['angle'] = data['Goals']/data['Goals'].sum() * 2 *pi
# data['color'] = Category20c[len(result_df)]

# # Create Figure
# p = figure(height=350, title="Venue", toolbar_location=None,
#            tools="hover", tooltips="Venue: @Venue, Goal: @Goals", x_range=(-0.5, 1.0))

# p.wedge(x=0, y=1, radius=0.4,
#         start_angle=cumsum('angle', include_zero=True), end_angle=cumsum('angle'),
#         line_color="white", fill_color='color', legend_field='Venue', source=data)

# # p.outline_line_color=None
# p.axis.axis_label = None
# p.axis.visible = False
# p.grid.grid_line_color = None

# show(p)

# Bokeh
from bokeh.plotting import figure, show
from bokeh.transform import cumsum
from bokeh.palettes import Category20c

# Embedding 
from bokeh.embed import components
from bokeh.resources import CDN

# Pandas
import pandas as pd

# Math
from math import pi

# Importing Dataset
df = pd.read_excel("data/ronaldo.xlsx")

# Data Analysis
result_df = df["Venue"].value_counts().reset_index()
result_df.columns = ['Venue', 'Goals']

data = result_df
data['angle'] = data['Goals'] / data['Goals'].sum() * 2 * pi
data["color"] = ["#3182bd","#6baed6"]

# Create Figure
p = figure(height=350, title="Venue", toolbar_location=None,
           tools="hover", tooltips="Venue: @Venue, Goal: @Goals", x_range=(-0.5, 1.0))

# Pie chart
p.wedge(x=0, y=1, radius=0.4,
        start_angle=cumsum('angle', include_zero=True), end_angle=cumsum('angle'),
        line_color="white",fill_color='color', legend_field='Venue', source=data)

# Styling
p.axis.axis_label = None
p.axis.visible = False
p.grid.grid_line_color = None

# show(p)

js2,div2 = components(p)
cdn_jss2 = CDN.js_files[0]