#Bokeh  
from bokeh.plotting import figure, show
from bokeh.models import ColumnDataSource
from bokeh.layouts import column

# Tooptip
from bokeh.models.tools import HoverTool

# Pandas
import pandas as pd

# Embedding 
from bokeh.embed import components
from bokeh.resources import CDN

df = pd.read_excel("data/ronaldo.xlsx")
result_df = df.groupby('Year')['Club'].agg(['count', lambda x: ', '.join(x.unique())]).reset_index()
result_df.columns = ['Year', 'Goals', 'Club']
df2 = result_df.sort_values(by='Year')

source = ColumnDataSource(data=df2)

# Figure
p = figure(
    title="Cristiano Ronaldo's Goals in Calendar Format",
    x_axis_label='Year', 
    y_axis_label='Goals', 
    height=400, 
    width=800)

# Graph
p.line(x='Year', y="Goals", source=source, legend_label="Goals", line_width=2)

# Tooltips : Hover Effect
hover = HoverTool()
hover.tooltips = """
    <div>
        <div style="padding:10px;">
            <h2>Stats</h2>
            <div><strong>Goals: </strong><span style="color: blue; font-weight: bold;">@Goals</span></div>
            <div><strong>Club: </strong><span style="color: blue; font-weight: bold;">@Club</span></div>
            <div><strong>Year: </strong><span style="color: blue; font-weight: bold;">@Year</span></div>
        </div>
    </div>
"""

# setting
p.legend.location = "top_left"
p.legend.click_policy = "hide"
p.add_tools(hover)

# Show
# show(p)
# layout1 = column(p)

jsQ1,divQ1 = components(p)
cdn_jssQ1 = CDN.js_files[0]