from bokeh.plotting import figure, show
from bokeh.models import ColorBar
from bokeh.transform import linear_cmap
from bokeh.palettes import Viridis256
from bokeh.io import output_file

import pandas as pd
import numpy as np

from bokeh.io import output_file, save

# Create example data
data = {
    'Minute': np.random.choice(range(1, 91), size=100),  # Random minutes between 1 and 90
    'Club': np.random.choice(['Real Madrid', 'Manchester United', 'Juventus'], size=100),
    'Goals': np.random.randint(0, 6, size=100),  # Random goals between 0 and 5
}

df = pd.DataFrame(data)

# Bokeh heatmap
output_file("heatmap.html")

# Create a heatmap
p = figure(title="Cristiano Ronaldo's Preferred Goal-Scoring Minutes",
           x_axis_label='Minute', y_axis_label='Club',
           toolbar_location=None, tools="", y_range=list(df['Club'].unique()))

# Use linear color mapping for the heatmap
mapper = linear_cmap(field_name='Goals', palette=Viridis256, low=df['Goals'].min(), high=df['Goals'].max())
p.rect(x='Minute', y='Club', width=1, height=1, source=df,
       line_color=None, fill_color=mapper)

# Add color bar
color_bar = ColorBar(color_mapper=mapper['transform'], width=8, location=(0, 0))
p.add_layout(color_bar, 'right')

# Show the plot
# show(p)

output_file("heatmap.html")
save(p)