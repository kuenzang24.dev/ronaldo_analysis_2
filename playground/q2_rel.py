# Bokeh
from bokeh.plotting import show, figure
from bokeh.models import ColumnDataSource, HoverTool, Select
from bokeh.layouts import column

# Pandas
import pandas as pd

# Importing Dataset
df = pd.read_excel("data/ronaldo.xlsx")

# Data
result_df = df['Minute'].value_counts().reset_index()
result_df.columns = ['Minute', 'Goals']
df2 = result_df.sort_values(by='Minute')

source = ColumnDataSource(data=df2)

# Get the list of unique clubs
unique_clubs = df['Club'].unique()

# Create a dropdown menu for club selection
club_select = Select(title="Select a Club:", value=unique_clubs[0], options=unique_clubs)

# Function to update the data source based on the selected club
def update_data(attrname, old, new):
    selected_club = club_select.value
    filtered_df = df[df['Club'] == selected_club]
    filtered_result_df = filtered_df['Minute'].value_counts().reset_index()
    filtered_result_df.columns = ['Minute', 'Goals']
    filtered_result_df = filtered_result_df.sort_values(by='Minute')
    source.data = ColumnDataSource.from_df(filtered_result_df)

# Update the data source when the club is selected
club_select.on_change('value', update_data)

# Figure
p = figure(
    title="Cristiano Ronaldo's Goal-Scoring Over Time",
    x_axis_label='Minute', 
    y_axis_label='Goals', 
    height=400, 
    width=800)

p.line(x='Minute', y="Goals", source=source, legend_label="Minute", line_width=2)

# Tooltips
hover = HoverTool()
hover.tooltips = """
    <div>
        <div style="padding:10px;">
            <h2>Stats</h2>
            <div><strong>Goals: </strong><span style="color: blue; font-weight: bold;">@Goals</span></div>
            <div><strong>Minute: </strong><span style="color: blue; font-weight: bold;">@Minute</span></div>
        </div>
    </div>
"""

# setting
p.legend.location = "top_left"
p.legend.click_policy = "hide"
p.add_tools(hover)

# Create a layout with the dropdown menu and the plot
layout = column(club_select, p)

# Show the plot
show(layout)
