# Bokeh
from bokeh.plotting import show
from bokeh.models import Div, CustomJS, Select
from bokeh.layouts import column

# Pandas
import pandas as pd

# Importing Dataset
df = pd.read_excel("data/ronaldo.xlsx")
df3 = df[['Venue', 'Club', 'Opponent', 'Playing_Position', 'Type',
       'Goal_assist', 'Year']]

# select setting
select_option = sorted(df['Year'].unique())

# Status Analysis function
def dataGetting(year):
    # Total Goal, Year, Club    
    general = df3.groupby('Year')['Club'].agg(['count', lambda x: ', '.join(x.unique())]).reset_index()
    general.columns = ['Year', 'Goals', 'Club']
    selective = general[general.Year == year]
    
    # Venue
    venue = df3[df3.Year == year].Venue.value_counts()
    
    # Type of Goal
    typeOfGoal = df3[df3.Year == year].Type.value_counts().reset_index()
    typeOfGoal.columns = ['Type', 'Goals']
    typeData = dict(zip(typeOfGoal['Type'], typeOfGoal['Goals']))
    
    # Opponent
    opponent = df3[df3.Year == year].Opponent.value_counts().reset_index().head(3)
    opponent.columns = ['Opponent', 'Goals']
    opponentData = dict(zip(opponent['Opponent'], opponent['Goals']))

    # Assist Provider
    assistProvider = df3[df3.Year == year].Goal_assist.value_counts().reset_index().head(3)
    assistProvider.columns = ['Assist', 'Goals']
    assistData = dict(zip(assistProvider['Assist'], assistProvider['Goals']))
    
    # Playing position
    position = df3[df3.Year == year].Playing_Position.value_counts()
    
    # Return Value
    data = {
        "General":{
            "Year":selective.iloc[0].Year,
            "Club":selective.iloc[0].Club,
            "Goals":selective.iloc[0].Goals
        },
        "Venue":{
            "Home":getattr(venue, 'H', 0),
            "Away":getattr(venue, 'A', 0)
        },
        "Opponent":opponentData,
        "Playing_Position": {
        "Left Wing": getattr(position, 'LW', 0),
        "Center Forward": getattr(position, 'CF', 0),
        "Right Wing": getattr(position, 'RW', 0),
        },
        "Assist":assistData,
        "Type":typeData
    },
    
    return data

# status Ui
def getUI(data):
    data = data[0]
    # data
    opponents_data = data["Opponent"]
    playing_positions_data = data["Playing_Position"]
    assist_data = data['Assist']
    type_data = data["Type"]
    
    opponent_section = ""
    playing_positions_section = ""
    assist_section = ""
    type_section = ""
    
    for opponent, goals in opponents_data.items():
        opponent_section += f"<p><strong>{opponent}</strong>{goals} goals</p>"

    for position, goals in playing_positions_data.items():
        playing_positions_section += f"<p><strong>{position}</strong>{goals} goals</p>"
    
    for assist_type, goals in assist_data.items():
        assist_section += f"<p><strong>{assist_type}</strong>{goals} goals</p>"

    for goal_type, goals in type_data.items():
        type_section += f"<p><strong>{goal_type}</strong>{goals} goals</p>"
        
    statusTemplate = f"""
        <p>Stats for the year: {data["General"]["Year"]}</p>
        <p><strong>Club: </strong>{data["General"]["Club"]}</p> 
        <p><strong>Total Goal: </strong>{data["General"]["Goals"]}</p> 
        <div>
            <h1>Venue</h1>
            <p><strong>Home : </strong>{data["Venue"]["Home"]} goals</p> 
            <p><strong>Away : </strong>{data["Venue"]["Away"]} goals</p> 
        </div>
        <div>
            <h1>Opponent</h1>
            {opponent_section} 
        </div>
        <div>
            <h1>Playing_Position</h1>
            {playing_positions_section}
        </div>
        <div>
            <h1>Goal_assist</h1>
            {assist_section} 
        </div>
        <div>
            <h1>Type</h1>
            {type_section}
        </div>
    """
    
    return statusTemplate 

# Div and select widget
div = Div(text=getUI(dataGetting(2002)), width=200, height=100)
select = Select(title="Option:", value=str(select_option[0]), options=[str(year) for year in select_option])

# Adding callback the select widget
# JS CALLBACK
# select.js_on_change("value", CustomJS(code="""
#     const selectedValue = cb_obj.value;
#     div.text = `"""+getUI(dataGetting(2005))+"""`;
# """, args= {"div":div}))

# PYTHON CALLBACK
def update_div(attr,old,new):
    selected_value = int(new)
    div.text = getUI(dataGetting(selected_value))

select.on_change("value", update_div)

# Creating layout
layout = column(select, div)

# Show Layout
show(layout)