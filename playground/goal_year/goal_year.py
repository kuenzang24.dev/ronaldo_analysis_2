#Bokeh  
from bokeh.plotting import figure, show
from bokeh.models import ColumnDataSource

# Data 
data = {
    'Year': [2010, 2011, 2012, 2013, 2014],
    'Club1': [25, 30, 20, 35, 28],
    'Club2': [18, 22, 25, 20, 30],
}

source = ColumnDataSource(data=data)

# Figure
p = figure(
    title="Cristiano Ronaldo's Goal-Scoring Over Time",
    x_axis_label='Year', 
    y_axis_label='Goals', 
    height=400, 
    width=800)

clubs = list(data.keys())[1:]
for club in clubs:
    # Graph
    p.line(x='Year', y=club, source=source, legend_label=club, line_width=2)

# setting
p.legend.location = "top_left"
p.legend.click_policy = "hide"

show(p)