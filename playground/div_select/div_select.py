# BASIC CONNECTION BETWEEN DIV AND SELECT WIDGET
# from bokeh.plotting import show
# from bokeh.models import Div,CustomJS, Select
# from bokeh.layouts import column,row

# div = Div(text="<p>Hello world</p>",width=200, height=100)
# select = Select(title="Option:", value="foo", options=["foo", "bar", "baz", "quux"])

# select.js_on_change("value", CustomJS(code="""
#     div.text = "<p>You made a selection!</p>"
# """,args={"div":div}))

# layout = column(select,div)

# show(layout)