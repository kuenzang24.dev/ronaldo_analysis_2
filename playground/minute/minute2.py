# Bokeh
from bokeh.plotting import show,figure
from bokeh.models import ColumnDataSource

# Pandas
import pandas as pd

# Importing Dataset
df = pd.read_excel("data/ronaldo.xlsx")

# Data 
result_df = df[df.Club == "Juventus FC"].Minute.value_counts().reset_index()
result_df.columns = ['Minute', 'Goals']
df2 = result_df.sort_values(by='Minute')

source = ColumnDataSource(data=df2)

# Figure
p = figure(
    title="Cristiano Ronaldo's Goal-Scoring Over Time",
    x_axis_label='Minute', 
    y_axis_label='Goals', 
    height=400, 
    width=800)

p.line(x='Minute', y="Goals", source=source, legend_label="Minute", line_width=2)

# setting
p.legend.location = "top_left"
p.legend.click_policy = "hide"

show(p)