from bokeh.plotting import figure, show
from bokeh.models import ColumnDataSource

# Your data
x = [1, 2, 3, 4, 5]
y = [2, 5, 8, 3, 7]

# Create a ColumnDataSource
source = ColumnDataSource(data=dict(x=x, y=y))

# Create a figure
p = figure(title="Line Chart with Dots", x_axis_label='X-axis', y_axis_label='Y-axis')

# Add a line glyph
line = p.line('x', 'y', source=source, line_width=2, legend_label="Line")

# Add a circle glyph for the dots
dots = p.circle('x', 'y', source=source, size=8, fill_color="red", line_color="black", legend_label="Dots")

# Show the plot
show(p)
