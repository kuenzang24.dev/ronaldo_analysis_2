# Bokeh
from bokeh.plotting import figure, show
from bokeh.palettes import Category20c

# Pandas
import pandas as pd

# Importing Dataset
df = pd.read_excel("data/ronaldo.xlsx")

# Data Analysis
result_df = df.groupby(['Opponent', 'Competition']).agg({'Goals': 'mean'}).reset_index()

# Create Figure
p = figure(height=350, width=800, title="Average Goals Against Opponents by League", toolbar_location=None,
           tools="hover", tooltips="Opponent: @Opponent, League: @League, Average Goals: @Goals", x_range=result_df['Opponent'])

# Color mapping
color_mapping = dict(zip(result_df['Competition'].unique(), Category20c[len(result_df['Competition'].unique())]))
result_df['color'] = result_df['Competition'].map(color_mapping)

# Plotting bars
p.vbar(x='Opponent', top='Goals', width=0.9, color='color', legend_field='League', source=result_df)

# Style the plot
p.title.text_font_size = '16px'
p.xaxis.major_label_orientation = "vertical"
p.xaxis.axis_label = "Opponent"
p.yaxis.axis_label = "Average Goals"
p.legend.title = "Competition"
p.grid.grid_line_color = None

show(p)
