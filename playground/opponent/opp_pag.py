# Bokeh
from bokeh.plotting import figure, curdoc,show
from bokeh.models import ColumnDataSource, Button
from bokeh.layouts import layout
from bokeh.palettes import Spectral11

# Pandas
import pandas as pd

# Importing Dataset
df = pd.read_excel("data/ronaldo.xlsx")

# Data Analysis
result_df = df["Opponent"].value_counts().reset_index()
result_df.columns = ['Opponent', 'Goals']

# Set the number of opponents per page
opponents_per_page = 10
current_page = 0

# Function to update the displayed data based on the current page
def update():
    global current_page
    start_idx = current_page * opponents_per_page
    end_idx = (current_page + 1) * opponents_per_page
    current_data = result_df.iloc[start_idx:end_idx]

    # Create a ColumnDataSource for the current data
    source.data = ColumnDataSource.from_df(current_data)

# Create a ColumnDataSource
source = ColumnDataSource(data=dict(Opponent=[], Goals=[]))

# Initial update
update()

# Create Figure
p = figure(y_range=result_df['Opponent'], height=400, title="Total Goals Scored by Ronaldo Against Each Opponent",
           tools="hover", tooltips="Goals: @Goals", x_axis_label="Total Goals")

# Horizontal Bar Chart
p.hbar(y='Opponent', right='Goals', height=0.8, source=source, line_color="white", fill_color=Spectral11[0])

# Styling
p.ygrid.grid_line_color = None
p.xaxis.axis_label_standoff = 12

# Create a Button to switch between pages
button = Button(label="Next Page", button_type="success")

# Define the callback function for the button
def button_callback():
    global current_page
    current_page = (current_page + 1) % (len(result_df) // opponents_per_page)
    update()

# Attach the callback function to the button
button.on_click(button_callback)

# Create the layout
layout = layout([[button], [p]])

# Add the layout to the current document
# curdoc().add_root(layout)

show(layout)
