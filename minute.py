# Bokeh
from bokeh.plotting import show,figure,save
from bokeh.models import ColumnDataSource

# Tooptip
from bokeh.models.tools import HoverTool

# Embedding 
from bokeh.embed import components
from bokeh.resources import CDN

# Pandas
import pandas as pd

# Importing Dataset
df = pd.read_excel("data/ronaldo.xlsx")

# Data 
result_df = df['Minute'].value_counts().reset_index()
result_df.columns = ['Minute', 'Goals']
df2 = result_df.sort_values(by='Minute')

source = ColumnDataSource(data=df2)

# Figure
p = figure(
    title="Cristiano Ronaldo's Goal-Scoring Over Time",
    x_axis_label='Minute', 
    y_axis_label='Goals', 
    height=400, 
    width=800)

p.line(x='Minute', y="Goals", source=source, legend_label="Minute", line_width=2)

# Tooltips
hover = HoverTool()
hover.tooltips = """
    <div>
        <div style="padding:10px;">
            <h2>Stats</h2>
            <div><strong>Goals: </strong><span style="color: blue; font-weight: bold;">@Goals</span></div>
            <div><strong>Minute: </strong><span style="color: blue; font-weight: bold;">@Minute</span></div>
        </div>
    </div>
"""

# setting
p.legend.location = "top_left"
p.legend.click_policy = "hide"
p.add_tools(hover)

save(p)

js,div = components(p)
cdn_jss = CDN.js_files[0]