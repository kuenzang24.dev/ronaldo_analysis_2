from flask import Flask, render_template
from bokeh.embed import server_session
from bokeh.client import pull_session

# Question 1
from goal_time import jsQ1,divQ1,cdn_jssQ1
# Question 2
from minute import js,div,cdn_jss
# Question 3
from position import js1,div1,cdn_jss1
from bargraph_q3 import js_bar_q3,div_bar_q3,cdn_jss_bar_q3
# Question 4
from venue import js2,div2,cdn_jss2
from bargraph_q4 import js_bar_q4,div_bar_q4,cdn_jss_bar_q4
# Question 5
from opponent import js3,div3,cdn_jss3
from q5_venue_top import js_q5_venue,div_q5_venue,cdn_jss_q5_venue

app = Flask(__name__)

@app.route("/")
def dashboard():
    return render_template("dashboard.html")

@app.route("/goal_time")
def stats():
    session = pull_session(url="http://localhost:5006/goal_over_time")
    bokeh_script = server_session(session_id=session.id,url="http://localhost:5006/goal_over_time")
    return render_template("goal_time.html",bokeh_script=bokeh_script)

@app.route("/minute")
def minute():
    return render_template("minute.html",js=js,div=div,cdn_jss=cdn_jss)

@app.route("/position")
def position():
    return render_template("position.html",js=[js1,js_bar_q3],div=[div1,div_bar_q3],cdn_jss=[cdn_jss1,cdn_jss_bar_q3])

@app.route("/venue")
def venue():
    return render_template("venue.html",js=[js2,js_bar_q4],div=[div2,div_bar_q4],cdn_jss=[cdn_jss2,cdn_jss_bar_q4])

@app.route("/opponent")
def opponent():
    return render_template("opponent.html",js=[js3,js_q5_venue],div=[div3,div_q5_venue],cdn_jss=[cdn_jss3,cdn_jss_q5_venue])

if __name__ == "__main__":
    app.run(debug=True)