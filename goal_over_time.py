from bokeh.plotting import figure, show

from graphs.goal_time import layout1
from graphs.goal_time_stats import layout2

from bokeh.io import curdoc
from bokeh.layouts import column

layout = column(layout1,layout2)
curdoc().add_root(layout)

# show(layout)
