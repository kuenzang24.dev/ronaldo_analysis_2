# Bokeh
from bokeh.plotting import figure, show, gridplot
from bokeh.models import FactorRange
from bokeh.palettes import Spectral11

# Embedding 
from bokeh.embed import components
from bokeh.resources import CDN

# Pandas
import pandas as pd

# Importing Dataset
df = pd.read_excel("data/ronaldo.xlsx")

# Get the top 10 opponents based on the number of matches played
top_10_opponents = df['Opponent'].value_counts().nlargest(10).index

# Create a subplot for each opponent
plots = []
for opponent in top_10_opponents:
    # Filter the DataFrame for matches involving the current opponent
    opponent_matches = df[df['Opponent'] == opponent]

    # Group by 'Venue' and count the number of matches
    matches_by_venue = opponent_matches.groupby('Venue').size().reset_index(name='Matches_Count')

    # Create Figure
    p = figure(x_range=FactorRange(*matches_by_venue['Venue']), height=350, title=f"Matches Count Against {opponent}",
               toolbar_location=None, tools="hover", tooltips="Matches Count: @Matches_Count", width=600)

    # Plotting bars
    p.vbar(x='Venue', top='Matches_Count', width=0.9, color=Spectral11[0], legend_label='Matches Count', source=matches_by_venue)

    # Style the plot
    p.title.text_font_size = '16px'
    p.xaxis.axis_label = "Venue"
    p.yaxis.axis_label = "Matches Count"
    p.xaxis.major_label_orientation = "horizontal"
    p.y_range.start = 0
    p.grid.grid_line_color = None

    # Add the plot to the list of plots
    plots.append(p)

# Create a grid layout for all 10 plots
grid = gridplot(plots, ncols=2, width=400, height=300)

# Show the grid layout
# show(grid)

js_q5_venue,div_q5_venue = components(grid)
cdn_jss_q5_venue = CDN.js_files[0]
