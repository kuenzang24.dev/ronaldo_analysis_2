# Bokeh
from bokeh.plotting import figure, show
from bokeh.transform import cumsum
from bokeh.palettes import Category20c
from bokeh.models import ColumnDataSource

# Embedding 
from bokeh.embed import components
from bokeh.resources import CDN

# Pandas
import pandas as pd

# Importing Dataset
df = pd.read_excel("data/ronaldo.xlsx")

# Data Analysis
result_df = df["Playing_Position"].value_counts().reset_index()
result_df.columns = ['Playing_Position', 'Goals']

source = ColumnDataSource(data=result_df)

p = figure(x_range=result_df["Playing_Position"], height=350, title="Goal Scored in diffferent playing position",
           toolbar_location=None, tools="")

p.vbar(x="Playing_Position", top="Goals", width=0.9,source=source)

p.xgrid.grid_line_color = None
p.y_range.start = 0

# show(p)

js_bar_q3,div_bar_q3 = components(p)
cdn_jss_bar_q3 = CDN.js_files[0]