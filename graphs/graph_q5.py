# Bokeh
from bokeh.plotting import figure, show
from bokeh.transform import cumsum
from bokeh.palettes import Category20c
from bokeh.models import ColumnDataSource

# Pandas
import pandas as pd

# Importing Dataset
df = pd.read_excel("data/ronaldo.xlsx")

# print(df.Opponent.value_counts())

# Common Competition
selected_opponent = 'Atletico de Madrid'
opponent_matches = df[df['Opponent'] == selected_opponent]
common_competition  = opponent_matches["Competition"].unique()
print(common_competition)

# Common Competition league
matches_count_by_competition = opponent_matches.groupby('Competition').size().reset_index(name='Goals Scored')
print(matches_count_by_competition)