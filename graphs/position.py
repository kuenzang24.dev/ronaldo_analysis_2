# Bokeh
from bokeh.plotting import figure, show
from bokeh.transform import cumsum
from bokeh.palettes import Category20c

# Pandas
import pandas as pd

# Math
from math import pi

# Importing Dataset
df = pd.read_excel("data/ronaldo.xlsx")

# Data Analysis
result_df = df["Playing_Position"].value_counts().reset_index()
result_df.columns = ['Playing_Position', 'Goals']

data = result_df
data['angle'] = data['Goals']/data['Goals'].sum() * 2*pi
data['color'] = Category20c[len(result_df)]

# Create Figure
p = figure(height=350, title="Playing Position", toolbar_location=None,
           tools="hover", tooltips="Playing_Position: @Playing_Position, Goal: @Goals", x_range=(-0.5, 1.0))

p.wedge(x=0, y=1, radius=0.4,
        start_angle=cumsum('angle', include_zero=True), end_angle=cumsum('angle'),
        line_color="white", fill_color='color', legend_field='Playing_Position', source=data)

# p.outline_line_color=None
p.axis.axis_label = None
p.axis.visible = False
p.grid.grid_line_color = None

show(p)