# Bokeh
from bokeh.plotting import show,figure
from bokeh.io import curdoc
from bokeh.models import Div, CustomJS, Select, ColumnDataSource
from bokeh.layouts import column

#Tooltip 
from bokeh.models.tools import HoverTool

# Pandas
import pandas as pd

# Importing Dataset
df = pd.read_excel("C:/Users/Kuenzang/OneDrive/Desktop/ronaldoProject/data/ronaldo.xlsx")

df3 = df[['Venue', 'Club', 'Opponent', 'Playing_Position', 'Type',
       'Goal_assist', 'Year']]

# select setting
select_option = sorted(df['Year'].unique())

# Status Analysis function
def dataGetting(year):
    # Total Goal, Year, Club    
    general = df3.groupby('Year')['Club'].agg(['count', lambda x: ', '.join(x.unique())]).reset_index()
    general.columns = ['Year', 'Goals', 'Club']
    selective = general[general.Year == year]
    
    # Venue
    venue = df3[df3.Year == year].Venue.value_counts()
    
    # Type of Goal
    typeOfGoal = df3[df3.Year == year].Type.value_counts().reset_index()
    typeOfGoal.columns = ['Type', 'Goals']
    typeData = dict(zip(typeOfGoal['Type'], typeOfGoal['Goals']))
    
    # Opponent
    opponent = df3[df3.Year == year].Opponent.value_counts().reset_index().head(3)
    opponent.columns = ['Opponent', 'Goals']
    opponentData = dict(zip(opponent['Opponent'], opponent['Goals']))

    # Assist Provider
    assistProvider = df3[df3.Year == year].Goal_assist.value_counts().reset_index().head(3)
    assistProvider.columns = ['Assist', 'Goals']
    assistData = dict(zip(assistProvider['Assist'], assistProvider['Goals']))
    
    # Playing position
    position = df3[df3.Year == year].Playing_Position.value_counts()
    
    # Return Value
    data = {
        "General":{
            "Year":selective.iloc[0].Year,
            "Club":selective.iloc[0].Club,
            "Goals":selective.iloc[0].Goals
        },
        "Venue":{
            "Home":getattr(venue, 'H', 0),
            "Away":getattr(venue, 'A', 0)
        },
        "Opponent":opponentData,
        "Playing_Position": {
        "Left Wing": getattr(position, 'LW', 0),
        "Center Forward": getattr(position, 'CF', 0),
        "Right Wing": getattr(position, 'RW', 0),
        },
        "Assist":assistData,
        "Type":typeData
    },
    
    return data

# status Ui
def getUI(data):
    data = data[0]
    # data
    opponents_data = data["Opponent"]
    playing_positions_data = data["Playing_Position"]
    assist_data = data['Assist']
    type_data = data["Type"]
    
    opponent_section = ""
    playing_positions_section = ""
    assist_section = ""
    type_section = ""
    
    for opponent, goals in opponents_data.items():
        opponent_section += f"<p><span style='opacity: 0.5;'>{opponent.upper()}</span> <strong>{goals} goals</strong></p>"

    for position, goals in playing_positions_data.items():
        playing_positions_section += f"<p><span style='opacity: 0.5;'>{position.upper()}</span> <strong>{goals} goals</strong></p>"
    
    for assist_type, goals in assist_data.items():
        assist_section += f"<p><span style='opacity: 0.5;'>{assist_type.upper()}</span> <strong>{goals} assists</strong></p>"

    for goal_type, goals in type_data.items():
        type_section += f"<p><span style='opacity: 0.5;'>{goal_type.upper()}</span> <strong>{goals} goals</strong></p>"
        
    statusTemplate = f"""
    <div>
        <h1 style="border-bottom:2px solid black;">#{data["General"]["Year"]}</h1>
        <div>
            <div style="display:flex; align-items: center; justify-content: center;">
                <img src="https://i.pinimg.com/originals/48/49/ba/4849ba2ea6517f805785071120cccc08.jpg" alt="ronaldo profile" style="width:305px; border-radius:5px;">
                <div style="margin-left:20px;">
                    <div>
                        <p>CRISTIANO</p>
                        <p style="font-size:48px; margin-top:-19px;">RONALDO</p>
                    </div>
                    <div>
                        <p><span style='opacity: 0.5;'>CLUB</span>  <strong>{data["General"]["Club"]}</strong></p> 
                        <p><span style='opacity: 0.5;'>GOALS</span>  <strong>{data["General"]["Goals"]} goals</strong></p> 
                        <p><span style='opacity: 0.5;'>HOME</span>  <strong>{data["Venue"]["Home"]} goals</strong></p> 
                        <p><span style='opacity: 0.5;'>AWAY</span>  <strong>{data["Venue"]["Away"]} goals</strong></p> 
                    </div>
                </div>
            </div>
            <div style="display:flex;">
                <div style="margin-right:20px">
                    <h3 style="border-bottom:2px solid black;">TOP 3 OPPONENT</h3>   
                    {opponent_section} 
                </div>
                <div style="margin-right:20px">
                    <h3 style="border-bottom:2px solid black;">PLAYING POSITION</h3>
                    {playing_positions_section}
                </div>
                <div style="margin-right:20px">
                    <h3 style="border-bottom:2px solid black;">TOP 3 ASSIST PROVIDER</h3>
                    {assist_section} 
                </div>
                <div>
                    <h3 style="border-bottom:2px solid black;">TYPE OF GOAL SCORED</h3>
                    {type_section}
                </div>
            </div>
        </div>
    </div>
    """
    
    return statusTemplate 

# Div and select widget
div = Div(text=getUI(dataGetting(2002)), width=1200, height=100)
select = Select(title="Select Year for Ronaldo's In-Depth Stats: ", value=str(select_option[0]), options=[str(year) for year in select_option])

# PYTHON CALLBACK
def update_div(attr,old,new):
    selected_value = int(new)
    div.text = getUI(dataGetting(selected_value))

select.on_change("value", update_div)

# Creating layout
layout2 = column(select, div)

# show(layout)
# just undo the below
# curdoc().add_root(layout)