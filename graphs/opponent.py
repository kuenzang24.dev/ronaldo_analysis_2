# Bokeh
from bokeh.plotting import figure, show
from bokeh.models import ColumnDataSource
from bokeh.palettes import Spectral11

# Pandas
import pandas as pd

# Importing Dataset
df = pd.read_excel("data/ronaldo.xlsx")

# Data Analysis
result_df = df["Opponent"].value_counts().reset_index()
result_df.columns = ['Opponent', 'Goals']
limited_df = result_df.head(10)

# Create a ColumnDataSource
source = ColumnDataSource(data=limited_df)

# Create Figure
p = figure(y_range=limited_df['Opponent'], height=400, title="Total Goals Scored by Ronaldo Against Each Opponent",
           tools="hover", tooltips="Goals: @Goals", x_axis_label="Total Goals")

# Horizontal Bar Chart
p.hbar(y='Opponent', right='Goals', height=0.8, source=source, line_color="white", fill_color=Spectral11[0])

# Styling
p.ygrid.grid_line_color = None
p.xaxis.axis_label_standoff = 12

show(p)
