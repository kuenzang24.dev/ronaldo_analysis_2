# Import necessary libraries
import pandas as pd
from bokeh.plotting import figure, show
from bokeh.io import output_file

# Sample DataFrame (replace this with your actual DataFrame)
data = {
    'Club': ['Club A', 'Club B', 'Club A', 'Club B', 'Club A', 'Club B'],
    'Minute': [10, 20, 15, 25, 30, 35]
}

df = pd.DataFrame(data)

# Output file for the Bokeh plot
output_file("goal_scoring_distribution.html")

# Create Bokeh plot
p = figure(title="Goal Scoring Distribution by Minute", x_axis_label="Minute", y_axis_label="Number of Goals", width=800, height=400)

# Group by Club and Minute, count goals, and plot line for each club
for club, color in zip(df['Club'].unique(), ['red', 'blue']):  # You can customize colors here
    club_data = df[df['Club'] == club].groupby('Minute').size().reset_index(name='Goals')
    p.line(club_data['Minute'], club_data['Goals'], line_width=2, line_color=color, legend_label=club)

# Customize plot
p.legend.title = 'Clubs'
p.legend.label_text_font_size = '10pt'
p.legend.title_text_font_size = '12pt'
p.legend.location = "top_left"

# Show plot
show(p)
