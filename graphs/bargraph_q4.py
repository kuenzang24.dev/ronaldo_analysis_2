# Bokeh
from bokeh.plotting import figure, show
from bokeh.transform import cumsum
from bokeh.palettes import Category20c
from bokeh.models import ColumnDataSource


# Pandas
import pandas as pd

# Importing Dataset
df = pd.read_excel("data/ronaldo.xlsx")

# Data Analysis
result_df = df["Venue"].value_counts().reset_index()
result_df.columns = ['Venue', 'Goals']

source = ColumnDataSource(data=result_df)

p = figure(x_range=result_df["Venue"], height=350, title="Goal Scored in diffferent Venue",
           toolbar_location=None, tools="")

p.vbar(x="Venue", top="Goals", width=0.9,source=source)

p.xgrid.grid_line_color = None
p.y_range.start = 0

show(p)